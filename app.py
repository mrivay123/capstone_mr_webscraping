#from flask import Flask, render_template
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from io import BytesIO
import base64
from bs4 import BeautifulSoup 
import requests

#don't change this
matplotlib.use('Agg')
app = Flask(__name__) #do not change this

#insert the scrapping here
url_get = requests.get('https://www.coingecko.com/en/coins/ethereum/historical_data?start_date=2022-01-01&end_date=2023-03-30#panel')
soup = BeautifulSoup(url_get.content,"html.parser")

#find your right key here
table = soup.find('tbody')
baris = table.find_all('tr')

row_length = len(baris)

temp = [] #initiating a list 
for i in range(1, row_length):
#insert the scrapping process here
    tanggal = table.find_all('tr')[i].th.text
    volume = table.find_all('td')[(i*4)+1].text.strip()
    temp.append((tanggal,volume)) 

temp = temp[::-1]

#change into dataframe
data = pd.DataFrame(temp, columns = ('tanggal','volume'))

#insert data wrangling here
df['volume'] = df['volume'].str.replace('$', '') # remove the '$' symbol
df['volume'] = df['volume'].str.replace(',', '') # remove the ',' separator
df['volume'] = df['volume'].astype('float64') # convert to float
ddf['tanggal'] = df['tanggal'].str.replace(',', '')
df['tanggal'] = df['tanggal'].astype('datetime64')
data = data.set_index('tanggal')
#end of data wranggling 

@app.route("/")
def index(): 
	
	card_data = f'{data["volume"].mean().round(2)}' #be careful with the " and ' 

	# generate plot
	ax = data.plot(figsize = (20,9)) 
	
	# Rendering plot
	# Do not change this
	figfile = BytesIO()
	plt.savefig(figfile, format='png', transparent=True)
	figfile.seek(0)
	figdata_png = base64.b64encode(figfile.getvalue())
	plot_result = str(figdata_png)[2:-1]

	# render to html
	return render_template('index.html',
		card_data = card_data, 
		plot_result=plot_result
		)


if __name__ == "__main__": 
    app.run(debug=True)